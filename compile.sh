#!/usr/bin/env bash

python -c "import pandocfilters" 1&> /dev/null

if [ $? == 1 ]; then
    echo ""
    echo "To use the filter written in Python, you need to install the Python module 'pandocfilters'"
    echo "  With pip:      sudo pip install pandocfilters"
    echo "  With ubuntu:   sudo apt-get install python-pandocfilters"
    echo "  With fedora:   sudo dnf install python-pandocfilters"
    echo ""
    exit 1
fi


pandoc --slide-level 3 --latex-engine=xelatex --filter "$PWD/columnfilter.py" slides.md -t beamer --template=template.beamer --tab-stop=2 -o slides.pdf
