---
title:  Introduction to Software Testing
shorttitle: Introduction to Software Testing
subtitle: MDI, ESIR3, TI-INFO
author:
- name: Erwan Bousse (TU Wien, Vienna, Austria)
  short: Erwan Bousse -- (CC BY-SA 4.0)
institute:
- name: ESIR, Rennes
  short: ESIR, Rennes
useshortauthors: true
toc: true
date: February 2018
section-titles: false
---


# Introduction

### About this course

- **Lecturer:** Erwan Bousse, researcher at TU Wien (Austria)
- **Contact:** erwan.bousse@tuwien.ac.at
- **Office:** Office E204 (green floor), IRISA, Campus de Beaulieu
- **Git repository:**\footnotesize{ \url{https://gitlab.inria.fr/ebousse/esir3-software-testing}}
  - Slides (markdown + \LaTeX)
  - Examples shown in the lecture
- You can contact me for:
  - asking questions on the course
  - asking questions on the lab assignment
  - giving feedback on the lecture or the slides
- Much content of this lecture come from the previous lectures made by Benoit Baudry, Benoit Combemale, and Arnaud Blouin


### From specification to system (I)

\begin{center}
\only<1-2>{\includegraphics[width=.8\textwidth]{figures/intro-1}}
\only<3>{\includegraphics[width=.8\textwidth]{figures/intro-2}}
\end{center}

- The developer reads the **specification**, delivers a **system**
\pause
- The client gives **inputs** to the system and reads **outputs**
\pause
- *Problem: what if outputs are incorrect?*


### Examples of incorrect outputs\ldots

\begin{tabular}{M{.5\textwidth}M{.5\textwidth}}

\includegraphics[width=.3\textwidth]{figures/fail1}

\textbf{USS Yorktown (1998):}

division by 0 disabled the engines

&
\includegraphics[width=.2\textwidth]{figures/fail2}

\textbf{Mars Climate Orbiter (1998):}

comparison of values with different units

\\
\includegraphics[width=.16\textwidth]{figures/fail3}

\textbf{Ariane V (1996):} 64-bit float converted to 16-bit integer

&
\includegraphics[width=.25\textwidth]{figures/fail4}

\textbf{Year 2000 problem:}

missing digits in date format

\\

\end{tabular}



### From specification to system (II)

\centering
\includegraphics[width=.8\textwidth]{figures/intro-3}

- Solution: the developer can **test** the system beforehand!
  - Give inputs to the system (clicking on buttons, giving data, \ldots)
  - Analyze outputs (screen, windows, console, side effects, database changes, \ldots) and **compare with the specification**
  - Repeat for each part of the specification



### Testing is difficult ''in the small''

~~~
while n > 1 do
  if n is even
    then n := n/2
    else n = 3*n+1
end while
ring alarm
~~~

- How to be sure that the alarm does ring for any `n`?
- 32-bit integer → 10 digits → 10$^{10}$ tests
- **6 lines of codes, 10 billions of cases!**

### Testing is difficult ''in the large'' (1)

Software systems are huge (lines of code):

- World of Warcraft server ~ 6 millions
- Android ~ 12 millions
- Open-office ~ 22 millions
- Microsoft Office 2013 ~ 42 millions
- Windows 10 ~ 60 millions
- Facebook ~ 60 millions
- Software on a modern car ~ 100 millions



### Testing is difficult ''in the large'' (2)

**Linux kernel:**


\includegraphics[width=.9\textwidth]{figures/linux-size}

### What can we do?

\pause

Let's learn testing \smiley!

# Software testing

### It all starts with a specification\ldots

A software system must fulfill a goal expressed with a **specification** describing everything that is expected from the system.

- An initial specification of the *complete system* is commonly made with the client in natural language
- During design and development, engineers specify *internal parts* of the system based on implementation choices, with:
  - coding rules ("all variables must have names"),
  - comments in the code,
  - contracts on operations (pre/post conditions, invariants),
  - UML models,
  - formal specifications (automata, mathematical equations),
  - \ldots

### Vocabulary -- what is testing?


\begin{definition}
A \textit{failure} is a behavior of the system that contradicts the specification (ie. when outputs are incorrect for some inputs).
\end{definition}

<!-- défaillance-->

\pause

\begin{definition}
A \textit{defect} (also called \textit{bug}) is an imperfection in the system causing the failure.
\end{definition}

<!-- défaut-->

\pause

\begin{definition}
\textit{Testing} is an investigation conducted to discover  defects in a system (or some part of a system).
\end{definition}



### Vocabulary -- example



~~~{.java}
private Label label;
public Button createButton() {
       Button btn = new Button();
       EventHandler handler = new EventHandler() {
           public void handle(ActionEvent event) {
                label.setText("Hello World!");
           }};
       return btn;
}
~~~

- **Specification**: a click on the button must show 'Hello World!'\pause
- **Failure**: clicking on the button does nothing\pause
- **Defect**: missing assignment `btn.setOnAction(handler)`\pause
- **Test**: the developer clicks on the button, observe that nothing happens and concludes that it contradicts the specification



### What do we test?

Everything we find in the specifications!

- Functionality
- Safety
- Security
- Usability
- Efficiency
- Consistency
- Maintainability
- \ldots



### When do we test?

Strongly depends on the *development process*:

- **Waterfall:** first implement everything, then prepare and apply tests (*greatly criticized*)
- **V model:** tests are prepared during the design phase, then applied after the implementation phase
- **Test-Driven Development (TDD):** first prepare tests, then implement and apply tests for each new feature in the system
- **Agile (or iterative):** testing is performed at each ''iteration''

\vfill
\pause


One simple rule to remember: the earlier tests are made, the earlier defects are found, the earlier it is to fix them


### Testing is not enough, but it improves *trust*


> *''Program testing can be quite effective for showing the presence of*
> *bugs, but is hopelessly inadequate for showing their absence.''*
> ---
> *Edsger Dijkstra*

\vfill
\pause

- Testing is necessary, BUT it is not a substitute for sound design and implementation!
- Testing improves **trust** that software works correctly, but can never certify that is works perfectly
- The best way to increase trust is to *increase the testing effort*



### Categories of testing approches

\pause

[columns]

[column=.5]

*How is the system investigated?*

- **static testing**: ''look'' at the system for defects without executing it
- **dynamic testing**: execute system with inputs, observe outputs

[column=.5]

\pause


*Who/what performs the test?*

- **manual testing**: each testing task is performed manually by a developer or tester
- **automatic testing**: each testing task is automatically performed by a program

[/columns]


### Categories of testing approaches: combinations



\begin{tabular}{|c|m{2.9cm}|m{4cm}|}
\hline
 & \textbf{Static} & \textbf{Dynamic}\\
\hline

\textbf{Manual} & code review (ie. reading the code) \cellcolor{darkred!10!white} & manually use the system and analyze outputs, as the client would do\\
\hline

\textbf{Automatic} & static analysis (eg. compilation errors)  \cellcolor{darkred!10!white}  & have a testing system use the system and analyze outputs for you \cellcolor{darkred!30!white} \\
\hline


\end{tabular}

\pause
\vfill

- *Warning:  ''automatic dynamic testing'' is often simply shortened to ''testing'', regardless of other categories!*
- **Scope of this lecture (in green):** mostly automated dynamic testing, with a few words on static testing



# Static testing

### Manual code review

*Idea:* finding bugs by having developers reading their code or the code of other developers of the project

\vfill

[columns]

[column=.51]

- **Very efficient:** in average, more than 50% of the defects of a project are found with code reviews, if any
- **Drawbacks:** costly to set up, time consuming, tiring for developers

[column=.49]

\centering
\includegraphics[width=\textwidth]{figures/wtf}

[/columns]



### Manual code review: example

[columns]

[column=.55]

\scriptsize

~~~{.java}
public int rech_rec(int[] L, int val,
                    int g, int d) {
  display(L,g,d);
  int rech_rec;
  int i,pt,dt;
  if (g < d) {
    pt = g + (d-g) / 3;
    if (val > L[pt]) {
      dt = (pt+1+d) / 2;
      if (val > L[pt]) {
        rech_rec=2+rech_rec(L, val, dt+1, d);
      } else  {
        rech_rec=2+rech_rec(L, val, pt+1, dt);
      }
    } else {
      rech_rec=1+rech_rec(L, val, g, pt);
    }
  } else {
    rech_rec=0;
  }
  return rech_rec;
}
~~~

[column=.45]

- No javadoc
- Horrible variable names
- Redundant `if` statement
- Unexplained call to `display`
- Conditionals not explained
- \ldots

[/columns]

<!-- calcule la complexité de la recherche de l'élément dans un tableau -->


### Automated code analysis

*Idea: a program that performs code review automatically*

\centering

\includegraphics[width=.8\textwidth]{figures/static-analysis}

- A static analyser parse the code, creates an Abstract Syntax Tree (AST), and visits the nodes of the AST
- Depending on the analysis, the report can contain:
  - Metrics (lines of code, cyclomatic complexity, coupling)
  - Unapplied coding rules
  - Inconsistencies (dead code, uninitialized variables, etc.)
- Some tools: compilers, PMD (Java), FindBugs (Java)

### Example: PMD report

\centering
\includegraphics[width=.8\textwidth]{figures/pmd-sample}

### Automated code analysis: benefits and limitations

*Benefits:*

- No tests to write, can be used "off-the-shelf"
- Helps improving code quality

\pause
\vfill

*Limitations:*

- Does not check the compliance with the specification, only finds problems that indirectly cause functionality problems
- Does not find all kinds of bugs:
  - automated tools give many *false positives* (wrong results)
  - very little behavioral verification

\pause
\vfill

Static testing must be complemented with **dynamic testing**!

# Dynamic testing

### Dynamic testing

- **Reminder:** dynamic testing relies on the execution of the program to investigate whether it complies with the specification
- *Core idea:* giving inputs to a program and observing its behavior
- Requires executing the same program a large amount of times to cover all relevant cases from the specification

### Dynamic testing: vocabulary

\begin{definition}
A \textit{test case} is a specification of the inputs and expected results required to execute a program and verify its compliance with some part of its specification.
\end{definition}

\begin{definition}
An \textit{oracle} is a procedure to check whether the results of an executed test case are compliant with the specification.
\end{definition}

\begin{definition}
A \textit{test suite} is a collection of test cases.
\end{definition}


### Dynamic testing: process

\centering
\includegraphics[width=\textwidth]{figures/dynamic-process}

### From manual to automated dynamic testing (I)

A test case can often be applied **manually**:

\vfill

\begin{center}
\includegraphics[width=.7\textwidth]{figures/dynamic-manual}
\end{center}

\vfill

However:

- it takes a lot of time,
- tedious to re-apply after each change in the program,
- some programs can only be used by other programs, not by humans (eg. frameworks)


### From manual to automated dynamic testing (II)

*Idea: writing each test case in the form of a small program, which automatically applies the test case when executed*

\vfill

\begin{center}
\includegraphics[width=.7\textwidth]{figures/dynamic-automated}
\end{center}

\vfill

- In other words: writing a program to test a program!
- Note that instead of writing a program per test case, it is possible to write a program per test suite

### Automated dynamic test case: example

[columns]

[column=.55]

\tiny

**\normalsize Program (Java):**

~~~{.java}
public class Palindrome {
  public static boolean isPalindrom(String word) {...}
  public static void main(String[] args) {
		boolean result = isPalindrom(args[0]);
		if (result) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}
	}
}
~~~

[column=.4]

\tiny

**\normalsize Test case (bash):**

~~~{.bash}
#!/bin/env bash

# Execute the program
RESULT=$(java -jar palindrome.jar "anna")

# Oracle
if [[ "$RESULT" == "Yes" ]]; then
  echo "Test [\"anna\", Yes] pass"
else
  echo "Test [\"anna\", Yes] failed"
fi
~~~


[/columns]

\vfill

**Result:**

~~~
$ bash ./test-case.sh
Test ["anna", Yes] pass
~~~

### Testing levels: from system to units

\centering

\only<1>{\includegraphics[width=.6\textwidth]{figures/dynamic-system-to-units-1}}
\only<2>{\includegraphics[width=.6\textwidth]{figures/dynamic-system-to-units-2}}

\pause

- A system can always be decomposed in **units**
- Usually the specification of a unit was made by a developer
- The definition of ''unit'' depends on the context
  - Procedural language (eg. C): commonly a function
  - Object-oriented (eg. Java): commonly a **class**


### Testing levels: system vs. unit testing

\centering

*Intuition: if each unit complies with its specification, it increases the chances of the whole system to comply with its specification!*

\includegraphics[width=.8\textwidth]{figures/dynamic-system-vs-unit-testing-1}

- Each unit can be tested in isolation as a ''small system''



### Unit testing

- *Benefits*:
  - **Facilitate the reuse of a unit**, because the quality of each unit is improved
  - **Facilitate fault localization**: if an defect is found, we directly know in which unit, contrarily to system testing
  - Direct application of the famous ''divide and conquer'' principle
- *Challenges:*
  - oracles must access the state of objects, which require accessors (eg. getters)
  - methods with visibility `package` are not easy to test
  - important to write testable code!

### Note on the following sections

Note that the remainder of this lecture ("Testing Frameworks", "Test Inputs" and "Integration Testing") is **only about dynamic testing!**

<!--
### White / black box
-->

# Testing frameworks

### Managing test cases and test suites is tedious

Writing test cases as programs can be tedious:

- **Oracle management:** each oracle must be programmed by hand, using comparison operators and conditionals
- **Side-effect management:** one test case must not impact the result of the execution of a following test case
- **Error management:** a test suite must continue even if a test case encounters an error (eg. an unhandled exception)
- **Results management:** must keep track of passed/failed tests, to provide a readable report for the developer


### Example of Java unit test suite (tedious to make)

[columns]

[column=.6]

\tiny

~~~{.java}
	public static void main(String args[]) {
		int passedTest = 0;
		int totalTests = 2;

    // Execution
		System.out.print("Test [\""+"anna"+"\","+"true"+"] ");
		boolean result = Palindrome.isPalindrom("anna");

		// Oracle
		if (result == true) {
			System.out.println("passed");
			passedTest++;
		} else {
			System.out.println("failed");
		}

		// Execution
    System.out.print("Test [\""+"john"+"\","+"false"+"] ");
		boolean result2 = Palindrome.isPalindrom("john");

		// Oracle
		if (result2 == false) {
			System.out.println("passed");
			passedTest++;
		} else {
			System.out.println("failed");
		}

		System.out.println("\nResult: "+passedTest+"/"+totalTests);
	}
}
~~~

[column=.4]

\footnotesize

**Output:**

~~~
Test ["anna",true] passed
Test ["john",false] passed

Result: 2/2
~~~

[/columns]

### Solution: test frameworks

Hopefully, it is possible to use **test frameworks**!

- Make the writing and running of tests **much easier**:
  - Operations called ''assertions'' are provided for the oracle
  - Each test is executed separately from others
  - If a test crashes, the test suite continues
  - Results are gathered and provided as a structured report
- Test framework for Java: **JUnit**
  - version 5 available since Sept. 2017
  - version 4 will still be used for MDI this year
  - **Not just for unit testing, but for any dynamic testing**

\vfill
\centering
\includegraphics[width=.25\textwidth]{figures/junit-logo}


### Same example rewritten with JUnit

[columns]

[column=.6]

\tiny


~~~{.java}
	@Test
	public void testAnna() {
		// Execution
		boolean result = Palindrome.isPalindrom("anna");
		// Oracle
		assertTrue(result);
	}

	@Test
	public void testJohn() {
		// Execution
		boolean result = Palindrome.isPalindrom("john");
		// Oracle
		assertFalse(result);
	}
~~~

[column=.4]

**Output:**

\includegraphics[width=\textwidth]{figures/junit-output}


[/columns]


### JUnit: core ideas

- The annotation `@Test` declares a method as a test case
- There can be multiple `@Test` methods per class
- **It is not necessary to define a `main` method**: JUnit provides a hidden `main` that executes all methods with the annotation `@Test`
  - With Eclipse, this `main` method is automatically used when choosing ''Run As... JUnit Test Case''
- A test case **must** contain an oracle using assertion methods or the `expected` parameter of `@Test` (see next slides)



### JUnit: assertions

JUnit provides many assertion methods to define oracles:

- `assertEquals(expected, observed);` ← checks that `expected` and `observed` are identical
- `assertNull(obj);` ← checks that `obj` is `null`
- `assertNotNull(obj);`  ← checks that `obj` is not `null`
- `assertFalse(bool);`  ← checks that `bool` is `false`
- `assertTrue(bool);` ← checks that `bool` is `true`
- `fail();` ← makes the test fail
- \ldots

\small
See <http://junit.sourceforge.net/javadoc/org/junit/Assert.html>

### JUnit: example of program and test cases

[columns]

[column=.5]

**\normalsize Program to test:**

\scriptsize

~~~{.java}
package myProject;

public class Number {
  int value;

  public Number(int val) {
    value = val;
  }

  public int multiply(int val) {
    return value*val;
  }
}
~~~
[column=.5]

**\normalsize Test cases:**

\scriptsize

~~~{.java}
package test;

import static org.junit.Assert.*;
import myProject.Number;
import org.junit.Test;

public class TestNumber {
  @Test
  public void testMultiply1() {
    Number nb = new Number(2);
    assertEquals(6, nb.multiply(3));
  }
  @Test
  public void testMultiply2() {
    Number nb = new Number(-1);
    assertEquals(-4, nb.multiply(4));
  }
}
~~~

[/columns]

### JUnit: managing oracles with exceptions

- If a method declares an exception with `throws`, it will throw an exception in some circumstances
- It is **necessary to test that the exception is thrown** in such circumstances
- But impossible to achieve with `assert`!
- *Solution*: the `expected` parameter in the `@Test` annotation declares that a test passes **only if the exception is thrown**

~~~{.java}
@Test(expected=MyException.class)
~~~

### JUnit: example with an exception

\footnotesize


**Method to test:**

~~~{.java}
public double divide(double v) throws NumberFormatException{
  if(Double.compare(v, 0.0) == 0)
    throw new NumberFormatException();
  return value/v;
}
~~~

**Test case**

~~~{.java}
@Test(expected=NumberFormatException.class)
public void testDivideZero() throws NumberFormatException {
    Number nb = new Number(2);
    nb.divide(0);  
}
~~~


### JUnit: managing redundant test inputs (1)

- Commonly, test cases share some identical test inputs
- Creates unwanted **redundancies**

~~~{.java}
public class TestNumber {
  @Test
  public void testMultiply() {
    Number nb = new Number(6); // same input
    assertEquals(2.0, nb.multiply(12.0);
  }
  @Test
  public void testDivide() {
    Number nb = new Number(6); // same input
    assertEquals(2.0, nb.divide(3.0));
  }
}
~~~

### JUnit: managing redundant test inputs (2)

- **Solution:** declare shared input data as attributes of the test class, initialized in a method with the `@Before` annotation
- A method with `@Before` is automatically called **before each test case** by JUnit

\tiny
\centering

~~~{.java}
public class TestNumber {
  Number nb;

  @Before
  public void setUp() {
    nb = new Number(6);
  }
  @Test
  public void testMultiply() {
    assertEquals(2.0, nb.multiply(12.0));
  }
  @Test
  public void testDivide() {
    assertEquals(2.0, nb.divide(3.0));
  }
}
~~~

### JUnit: some good practices

- Test cases should be stored separately from the system code.
  - Either in the same project, but **in a separate source folder** (create a new folder `test`, then right click on project → Properties → Java Build Path → Source → Add Folder → choose `test`)
  - Or **in a different project**
- When doing unit testing:
  - Create one test class per tested class
  - Create at least one test case per public method


### JUnit: configuring an Eclipse project

Right-click on the project with the tests → Properties → Java Build Path → Libraries → Add Library → JUnit → JUnit 4

\begin{center}
\includegraphics[width=.8\textwidth]{figures/junit-eclipse-library}
\end{center}

### JUnit: creating and running a test cases

- **Creating a class containing test cases**: Right-click on a folder → New → JUnit Test Case
- **Running the tests contained in the class**: Right-click on the class → Run As → JUnit Test

\vfill

- **Default content of the class:**

\includegraphics[width=\textwidth]{figures/junit-test-default}


# Test inputs

### Choosing relevant inputs

- A test case is mostly characterized by a *set of inputs* given to the tested program
- However, a program may sometimes take a large and almost infinite amount of combinations of inputs
- **How to choose the inputs of a test case?**

\vfill
\pause

- Two main families of techniques:
  - Functional testing (also called black-box testing)
  - Structural testing (also called white-box testing): not seen in this lecture


### Functional testing (or black-box testing)

- Only relies on:
  - the *specification* of the tested program
  - the *input domain* of the tested program (eg. if the program is a method with an integer parameter, then the input domain is the set of all integers)
- **Goal:** choose relevant inputs to check that the specification is fulfilled both regarding *functionality* and *error management*
- **Challenge:** impossible to explore complete input domains, must delimit the values to use

\vfill
\pause

Main techniques:

- Equivalent Class Partitioning
- Boundary Value Analysis

### Equivalent Class Partitioning

- *Idea:* partition the input domain in  **equivalent classes**
- For each equivalent class, the program is supposed to achieve a similar result, hence it is not necessary to make multiple test cases per equivalent class
- Then, pick one value (or a tuple of values if the input domain has several dimensions) per equivalent class, and make a test case per value

\vfill

\centering
\includegraphics[width=.6\textwidth]{figures/eq-classes}



### Boundary Value Analysis

- *Idea:* failures often happen "at the limits" (eg. divide by zero) of input domains
- For each input parameter of the tested program:
  - find the **boundaries** of the parameter
  - pick input values at and around the boundaries
  - Example for the domain [1, 100], pick: 1, 100, 2, 99, 0, 101
- Also applies to the boundaries of the *equivalent classes*

\vfill

\centering
\includegraphics[width=.6\textwidth]{figures/boundaries}

### Example

Example of a program to analyze triangles:

- **Inputs:** three real numbers, each a length of a side
- **Specification:**
  - if such lengths are invalid for a triangle, print *error*
  - else return a pair of two values:
    - one telling if the triangle is *isosceles*, *equilateral* or *scalene*
    - one telling if the biggest angle is *oblique*, *acute*, or *obtuse*

### Example: equivalent class partitioning

Eight equivalent classes, hence eight test cases:

1. lengths leading to (isosceles,oblique) --- eg. √2,2,√2
2. lengths leading to (isosceles,acute) --- eg. 6,1,6
3. lengths leading to (isosceles,obtuse) --- eg. 7,4,4
4. lengths leading to (scalene,oblique) --- eg. 3,4,5
5. lengths leading to (scalene,acute) --- eg. 6,5,3
6. lengths leading to (scalene,obtuse) --- eg. 5,6,10
7. lengths leading to (equilateral,acute) --- eg. 4,4,4
8. lengths leading to error --- eg. 1,1,2

### Example: boundary value analysis

Many possible test cases at the boundaries:

- 0, 0, 0: the triangle is a single point
- 4, 0, 3: one length is zero
- -3, -3, 5: negative values
- 0.001, 0.001, 0.001: very small triangle
- 88888, 88888, 88888: very big triangle
- 1, 2, 3.00001: almost a triangle
- 3.00001, 3, 3: almost equilateral
- 2.99999, 3, 4: almost isosceles
- 3, 4, 5.00001: almost oblique
- 5, 5, A: a letter as a length
- \ldots

# Integration testing

### From unit testing to integration testing
<!--
\includegraphics[width=.8\textwidth]{figures/dynamic-integration}
-->

- Unit testing focuses on one unit (eg. a class) at a time
- But units have relationships: one class A may use a class B
- **Problem:** if B has defects, then test cases of A will fail, even if A has no defects, which creates a fault localization problem.

\vfill
\pause

- **Solution:** we must *order* the units based on dependencies!
  - first we test B, then we test A
  - if the test cases of B fail, then the defects are in B
  - If the test cases of A fail, and the test cases of B pass, then the defects are in A

\vfill
\pause

*Integration testing* = ordering and adapting *unit test cases*



### Example of acyclic integration testing (1)

[columns]

[column=.6]

\scriptsize

~~~{.java}
class MyList {
	...
	private List<IElement> elements;
  public IElement getAfter(Element e) {...}
	public String toString() {
    String result = elements.stream()
      .map(i -> i.getLabel())
      .collect(Collectors.joining(", "));
		return result;
	}
}
~~~

[column=.4]

\scriptsize

~~~{.java}
class Element
    implements IElement{
	private String label;
	public String getLabel() {
		return label;
	}
}
~~~

[/columns]

\vfill
\pause

First we test `Element` to trust `getLabel`, then we test `MyList`.


### Example of acyclic integration testing (2)

\centering

\includegraphics[width=.9\textwidth]{figures/integration-example-acyclic}

\vfill
\pause

- Partial order for testing each unit: F, (C, D), E, B, A


### Problem: Cyclic dependencies

- We add an operation `getNext` to `Element`.

~~~{.java}
class Element implements IElement {
  ...
  private MyList parentList;
  public IElement getNext() {
    return parentList.getAfter(this);
  }
}
~~~

\pause

- Problem: we have a **cyclic dependency**: `MyList` requires `Element`, and `Element` requires `MyList`
  - if a test case of `MyList` fails, the defect could be in `Element`\ldots
  - \ldots but a defect in `Element` could be caused by `MyList`!
  - fault localization becomes impossible

### Breaking cycles using stubs

[columns]

[column=.6]

- *Idea:* implement a ''fake'' simplest version of one class, called a *stub*, and use it to test the other class
- A **stub** is a class that implements the same interface as some existing class, but simplifies as much as possible the body of methods (eg. constants)
- Each test case may require a different stub, depending on the required methods and values
\vfill

[column=.4]

\includegraphics[width=\textwidth]{figures/mylist-cd}

[/columns]

### Example of stubs for IElement

[columns]

[column=.5]

\scriptsize

**Element class:**

~~~{.java}
public class Element
    implements IElement {
	private String label;
	private MyList parentList;
  public Element(String label) {
    this.label = label;
  }
  public void setParent(MyList list) {
    this.parentList = list;
  }
	public IElement getNext() {
		return parentList.getAfter(this);
	}
	public String getLabel() {
		return label;
	}
}

~~~

[column=.5]

\scriptsize

**Example of stub 1**

~~~{.java}
public class ElementStub1
    implements IElement {
  public ElementStub1(String label) {}
  public void setParent(MyList list) {}
  public IElement getNext() {
    return null;
  }
  public String getLabel() {
    return "foo";
  }
}
~~~

**Example of stub 2**

~~~{.java}
public class ElementStub2
    implements IElement {
  ... /* same as ElementStub1 */
  public String getLabel() {
    return "bar";
  }
}
~~~

[/columns]

### Example of test case with stubs

\small

~~~{.java}
@Test
public void testToString() {

	// Prepare input data (without Element, only stubs)
	IElement element1 = new ElementStub1();
	IElement element2 = new ElementStub2();
	MyList list = new MyList(Arrays.asList(element1, element2));

	// Execution
	String result = list.toString();

	// Oracle
	assertEquals("foo, bar", result);
}
~~~

### Using stubs in a unit testing process

Testing `Account` and `Person` now requires three test suites:

1. Test real `MyList` class with a `Element` stub
2. Test real `Element` class with real `MyList` class
3. Test real `MyList` class with real `Element` class

\vfill
\pause

We can now localize faults efficiently:

- if the test suite 1 fails: defect in `MyList`
- if the test suite 1 passes and 2 fails: defect in `Element`
- if the test suites 1 and 2 pass, and  3 fails (rare): defect in the interactions between `MyList` and `Element`

### Note on unit tests ordering

- By default **JUnit executes unit tests in an unknown order**
  - In this case, the most important is to *read results in the correct order* in order to localize a fault correctly
  - Example (acyclic case): even if `MyList` is tested first, look at test results from `Element` first, because if they do not pass then test results of `MyList` are useless
- Optionally, JUnit provides an annotation `@FixMethodOrder` that can be used to run test cases in a specific order (eg. alphabetically)

### Other uses for stubs, outside integration testing

- When some code is **non-deterministic** (eg. using the `Random` class), a stub can replace the corresponding class and provide deterministic behavior for testing purposes
- When some code depends on **external resources** (eg. files, databases, web api), a stub can be used to provide a ''fake'' access to such resources, to be able to test this code without starting a database or creating files
- When some code requires a class that was **not implemented yet**, a stub can temporarily be used to replace this class for testing purposes

### Easier stubs with Mockito

- Manually creating stub classes is tedious, and quickly lead to a large amount of classes to manage
- *Solution: rely on dedicated frameworks to create stubs!*
- **Mockito** is the most known in Java, and can be used to create fine-grained stubs ''on the fly'' at the beginning of a test case, based on the methods required by the test case
- In Mockito, we define stubs using a specific kind of object called *mock*.

\vfill
\centering
\includegraphics[width=.4\textwidth]{figures/mockito}



### Mockito basics

- The test class must be annotated with `@RunWith(MockitoJUnitRunner.class)` to inform JUnit that Mockito is being used
- Mockito static methods must be imported: `import static org.mockito.Mockito.*;`
- Then **a mock can be created in two ways**:
  - Using the `mock(MyClass.class)` operation
  - Using the `@Mock` annotation (only works on an attribute)
- Finally **a mock must be configured** with behavior:
  - To configure that the mock must return 12 when `myMethod` is called: `when(myObject.myMethod()).thenReturn(12);`


### Example of Mockito usage

\scriptsize

~~~{.java}
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
...

@RunWith(MockitoJUnitRunner.class)
public class MyListTest_WithMockitoStub {

	@Test
	public void test() {
		// Prepare input data
		IElement element1 = mock(IElement.class);
		IElement element2 = mock(IElement.class);
		MyList list = new MyList(Arrays.asList(element1, element2));
		when(element1.getLabel()).thenReturn("foo");
		when(element2.getLabel()).thenReturn("bar");

		// Execute
		String result = list.toString();

		// Oracle
		assertEquals("foo, bar", result);
	}

}
~~~

### Setting up Mockito in an Eclipse project using Maven

- Mockito is unfortunately not available in Eclipse as a *Library*, contrary to JUnit
- The best alternative is to rely in the **Maven build system**
- You will need an **Eclipse Maven project**:
  - If you don't have one, create a Java project for your tests
  - Then convert it to a Maven project, *right click → Configure → Convert to Maven Project*
- Add this to the `pom.xml` file of your projet:

[block]

\footnotesize

~~~{.xml}
<dependencies>
	<dependency>
		<groupId>org.mockito</groupId>
		<artifactId>mockito-core</artifactId>
		<version>2.13.0</version>
	</dependency>
</dependencies>
~~~

[/block]

### Setting up Mockito in an Eclipse project using Maven (2)

\includegraphics[width=\textwidth]{figures/mockito-maven}

<!--

# Going further

## Testing tests

### Coverage

### Mutation

## Structural testing (or white-box testing)

- Idea: make test cases relevant for the structure of the program and the overall architecture

## From coding to testing

### Writing testable code

private VS public

dependency injection

### Test driven development

### Continuously adding test cases

-->
