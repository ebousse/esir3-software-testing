

# Tool reuse

## DSL diversity

## Metalanguage diversity

## Towards generic interfaces?

- To analyze languages
- To analyze models
- To analyze executions




# Tool specialization

## Generic is also bad

## Towards better tool configuration?

- Enriched DSLs with data
- Configurable tools, that still work out of the box, but improve with more available data


# Tool composition

## DSLs are composed together

## Towards tool composition

## Many development activities

## Towards tools collaborating accross activities

(goes beyond tools)

## Idea

Can we provide tools that are compatible (ie. reusable) with a wide range of DSLs?

- **Generators?** (ie. tools that derive tools from the language)
- **Design-time tools?** (ie. tools that provide services before the execution of models)
- **Runtime tools?** (ie. tools that provide services during the execution of models)

Implies that:

- Both the abstract syntax and the semantics must be analyzed
- At design time (of the language)
- At design time (of the model)
- At runtime (of the model)
- Execution must be analyzed (at runtime)
- Requires a "white-box" language definition


## My contributions

Contributions of reusable tools:

- Generator of language-specific trace management facilities (ECMFA+SoSym)
- Runtime trace constructor (SLE+JSS)
- Runtime omniscient debugger (SLE+JSS)




All these tools can concretely be reused "out of the box" for several very different executable DSLs (ie. DSLs with very different concepts and semantics)
### Problem #1
Reusable tools are necessarily rather generic, and therefore not tailored for the domain of the DSL of interest (hence quite contradictory with the DSM philosophy)


bien présenter tous les papiers et les années, et ne pas oublier de papiers


## DSL and Tooling Diversity

- Wide range of possible DSLs!
- Various **types of science and engineering**: software engineering, physics, systems engineering, data science, chemistry, mechanical engineering, robotics, electrical engineering, etc.
- Various **paradigms**: business processes, statements, protocols, orchestrations, activities, differential equations, functions, etc.
- Various **applications domains**: video-on-demand, resource management, avionics, hospital management, business analytics, etc.


- Wide range of possible **tools**
- Editors, interactive debuggers, traceability management, tracers, solvers, runtime monitoring, checkers, test runners, ...

data science, machine learning, computational stuff
DSL globalization?

## Summary

- **DSL diversity**
- One system is defined with many composed heterogeneous DSLs
- The development process of the system is itself a system, hence also defined with many composed heterogeneous DSLs

- **Tool diversity**: Each DSL requires its own set of tools
- Hence, HUGE tool development + composition problem!
- Huge amount of DSLs to develop
- System design level: composition of tools of different DSLs
- Developmement process level: composition of tools of different DSLs
- In between level: integrating tools for process management, with tools for system development
-->

 Elements of the development process must reference elements of the system (eg. a requirement pointing to the part that fulfills the requirement)


## Reuse in SLE

Two main axes:

- **Language Reuse**
- Language variability management
- Language modularization
- Language composition
- Language customization
- ...
- Or **Tool Reuse**, ie. **making tools that can be reused among DSLs**


Here we are interested in the latter

-->

<!--
meilleure transition:
language is sofware too
Separation des préoccupations côté modèle... et donc côté langage!
Langage == système
donc c'est naturel qu'on ait de la diversité au niveau métalangages
-->

## Meta-languages diversity

- There is not only a wide range of different possible DSLs...
- ... but also *a wide range of approaches to define DSLs*!
- **Abstract syntax** definition (grammar VS metamodel)
- **Semantics definition** (operational VS translational)
- **Execution state** definition (weaved , separate, rewriting)
- **Metalanguages** (metamodeling / model transformation / programming languages)
- **Design patterns** / methods (eg. visitor, rewriting, etc.)
- **Execution platform** (JVM, OS, embedded device, etc.)
- And diversity/choice is great:
- Can pick the use tools we are **familiar**/knowledgeable with
- Can aim for specific **good properties** (eg. SOS to prove properties, CCSL to have a concurrency model, Scala for a strong type system etc.)
- Can target a specific **technical environment** (Java to benefit from the JVM, C to target embedded devices, etc.)
-->

## Hence new problems


### Problem #2

Makes it even harder to provide reusable tools!

- One tool compatible with a specific metalanguage (eg. Ecore) works for all DSLs made with such metalanguage, but not for DSLs made with another metalanguage (eg. Monticore)

### Problem #3

Once metalanguages have been chosen, hard to switch to others

- Mathematical semantics redefined into code for execution
- Java-based semantics to C/C++ for embedded devices
- An interpreted DSL might become a compiled DSL


## Research directions (I)

### Research direction #1

Providing generic tools that can progressively/optionally be customized for a domain (see "Moldable" vision)

- Tools should still work "out of the box"
- But additional information at the language level should be able to configure these tools (eg. customized domain-specific "variable view" in a debugger)
- More information is added, more tailored is the tool! With a whole specter of possible configurations between "none" and "fully configured"

*Preliminary work: customization of trace metamodel generators*


## Research directions (II)

### Research direction #2.1
Identifying a set of "abstract "generic interfaces (or views) to analyze/understand the content of DSLs

- See the abstract syntax as a set of "concepts"
- See the semantics as a set of "rules"

*Preliminary work: prototype of a pivot metamodel to generically analyze operational semantics to generate trace metamodels for K3/xMOF*



## Research directions (III)

### Research direction #2.2
Identifying a set of generic interfaces to interact (control, observe) with executed models:

- See (and manipulate) the execution state through a standard data structure/API
- See and control the execution events/steps in a generic way
- See and emit domain-specific events in a generic way

*Preliminary work: a Java-only framework for the GEMOC Studio to observe executed models generically*

## Research directions (IV)

### Research direction #3
Facilitating the transformation of syntax/semantics from one metalanguage to another, ie. "define once, reuse everywhere"
- (note: this is actually language reuse, not tool reuse...)
