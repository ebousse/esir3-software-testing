package number;

public class Number {
	int value;

	public Number(int val) {
		value = val;
	}

	public int multiply(int val) {
		return value * val;
	}

	public double divide(double v) throws NumberFormatException {
		if (Double.compare(v, 0.0) == 0)
			throw new NumberFormatException();
		return value / v;
	}
}