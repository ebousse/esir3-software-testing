package number.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import number.Number;

public class TestNumber {

	Number nb;

	@Before
	public void setUp() {
		nb = new Number(2);
	}

	@Test
	public void testMultiply1() {
		assertEquals(6, nb.multiply(3));
	}

	@Test
	public void testMultiply2() {
		assertEquals(-2, nb.multiply(-1));
	}

	@Test(expected = NumberFormatException.class)
	public void testDivideZero() throws NumberFormatException {
		nb.divide(0);
	}
}
