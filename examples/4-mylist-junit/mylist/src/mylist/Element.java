package mylist;

public class Element implements IElement {
	private String label;
	private MyList parentList;

	public Element(String label) {
		this.label = label;
	}
	
	@Override
	public void setParent(MyList list) {
		this.parentList = list;
	}

	@Override
	public IElement getNext() {
		return parentList.getAfter(this);
	}

	@Override
	public String getLabel() {
		return label;
	}
}
