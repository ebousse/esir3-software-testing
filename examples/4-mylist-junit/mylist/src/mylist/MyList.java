package mylist;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class MyList {
	private List<IElement> elements;

	public MyList(Collection<IElement> elements) {
		this.elements = new ArrayList<IElement>();
		for (IElement element : elements) {
			this.elements.add(element);
			element.setParent(this);
		}
	}

	public IElement getAt(int index) {
		return elements.get(index);
	}

	public IElement getAfter(IElement element) {
		int nextIndex = elements.indexOf(element) + 1;
		return elements.get(nextIndex);
	}

	public String toString() {
		String result = elements.stream().map(i -> i.getLabel()).collect(Collectors.joining(", "));
		return result;
	}
}
