package mylist;

public interface IElement {

	IElement getNext();

	String getLabel();

	void setParent(MyList list);

}