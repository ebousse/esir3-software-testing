package mylist.tests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import mylist.Element;
import mylist.IElement;
import mylist.MyList;

public class MyListTest_NoStub {
	
	@Test
	public void testToString() {
		
		// Prepare input data
		IElement element1 = new Element("foo");
		IElement element2 = new Element("bar");
		MyList list = new MyList(Arrays.asList(element1,element2));
		
		// Execution
		String result = list.toString();
		
		// Oracle
		assertEquals("foo, bar", result);
	}

}
