package mylist.tests.stubs;

import mylist.IElement;
import mylist.MyList;

public class ElementStub1 implements IElement {

	@Override
	public IElement getNext() {
		return null;
	}

	@Override
	public String getLabel() {
		return "foo";
	}

	@Override
	public void setParent(MyList list) {
	}

}
