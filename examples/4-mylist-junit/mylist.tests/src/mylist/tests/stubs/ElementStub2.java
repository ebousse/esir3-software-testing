package mylist.tests.stubs;

import mylist.IElement;
import mylist.MyList;

public class ElementStub2 implements IElement {

	@Override
	public IElement getNext() {
		return null;
	}

	@Override
	public String getLabel() {
		return "bar";
	}

	@Override
	public void setParent(MyList list) {
		
	}

}
