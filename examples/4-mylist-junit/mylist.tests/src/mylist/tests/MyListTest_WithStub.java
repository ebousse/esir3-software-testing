package mylist.tests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;

import org.junit.Test;

import mylist.IElement;
import mylist.MyList;
import mylist.tests.stubs.ElementStub1;
import mylist.tests.stubs.ElementStub2;

public class MyListTest_WithStub {
	@Test
	public void testToString() {

		// Prepare input data
		IElement element1 = new ElementStub1();
		IElement element2 = new ElementStub2();
		MyList list = new MyList(Arrays.asList(element1, element2));

		// Execution
		String result = list.toString();

		// Oracle
		assertEquals("foo, bar", result);
	}

}
