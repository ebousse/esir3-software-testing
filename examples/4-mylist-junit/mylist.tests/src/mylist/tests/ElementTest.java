package mylist.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import mylist.Element;

public class ElementTest {
	@Test
	public void testGetLabel() {

		// Prepare input data
		Element element = new Element("foo");

		// Execution
		String result = element.getLabel();

		// Oracle
		assertEquals("foo", result);

	}
}
