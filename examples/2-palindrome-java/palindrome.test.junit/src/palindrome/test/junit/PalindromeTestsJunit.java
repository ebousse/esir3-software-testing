package palindrome.test.junit;

import org.junit.Test;
import palindrome.Palindrome;

import static org.junit.Assert.*;

public class PalindromeTestsJunit {
	
	@Test
	public void testAnna() {
		// Execution
		boolean result = Palindrome.isPalindrom("anna");
		// Oracle
		assertTrue(result);
	}
	
	@Test
	public void testJohn() {
		// Execution
		boolean result = Palindrome.isPalindrom("john");
		// Oracle
		assertFalse(result);
	}
}
