package palindrome.tests.nojunit;

import palindrome.Palindrome;

public class PalindromeTests {
	
	public static void main(String args[]) {
		
		int passedTest = 0;
		int totalTests = 0;
		
				
		System.out.print("Test [\""+"anna"+"\","+"true"+"] ");
		totalTests++;
		
		// Execution
		boolean result = Palindrome.isPalindrom("anna");
		
		// Oracle
		if (result == true) {
			System.out.println("passed");
			passedTest++;
		} else {
			System.out.println("failed");
		}
		
		
		
		System.out.print("Test [\""+"john"+"\","+"false"+"] ");
		totalTests++;
		
		// Execution
		boolean result2 = Palindrome.isPalindrom("john");
		
		// Oracle
		if (result2 == false) {
			System.out.println("passed");
			passedTest++;
		} else {
			System.out.println("failed");
		}
		
		
		System.out.println("\nResult: "+passedTest+"/"+totalTests);
	}
}
