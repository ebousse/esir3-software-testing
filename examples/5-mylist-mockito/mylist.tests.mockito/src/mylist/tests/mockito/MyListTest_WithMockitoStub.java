package mylist.tests.mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import mylist.IElement;
import mylist.MyList;

@RunWith(MockitoJUnitRunner.class)
public class MyListTest_WithMockitoStub {

	@Test
	public void test() {
		// Prepare input data
		IElement element1 = mock(IElement.class);
		IElement element2 = mock(IElement.class);
		MyList list = new MyList(Arrays.asList(element1, element2));
		when(element1.getLabel()).thenReturn("foo");
		when(element2.getLabel()).thenReturn("bar");

		// Execute
		String result = list.toString();

		// Oracle
		assertEquals("foo, bar", result);
	}

}
