package palindrome;

public class Palindrome {
	public static boolean isPalindrom(String word) {
		int i1 = 0;
		int i2 = word.length() - 1;
		while (i2 > i1) {
			if (word.charAt(i1) != word.charAt(i2)) {
				return false;
			}
			i1++;
			i2--;
		}
		return true;
	}

	public static void main(String[] args) {
		boolean result = Palindrome.isPalindrom(args[0]);
		if (result) {
			System.out.println("Yes");
		} else {
			System.out.println("No");
		}
	}
}
