#!/bin/env bash

RUNPALINDROME="java -jar palindrome.jar"

PASSED=0
NBCASES=0

function run_test {
  let NBCASES="$NBCASES+1"

  # Execute the program
  RESULT=$($RUNPALINDROME "$1")

  # Oracle
  if [[ "$RESULT" == "$2" ]]; then
    echo "- Test [\"$1\", $2] pass"
    let PASSED="$PASSED+1"
  else
    echo "- Test [\"$1\", $2] failed"
  fi
}

# Empty string
#    - Input: ""
#    - Expected: "Yes"
run_test "" "Yes"

# Single letter
#    - Input: "a"
#    - Expected: "Yes"
run_test "a" "Yes"

# Simple palindrome
#    - Input: "anna"
#    - Expected: "Yes"
run_test "anna" "Yes"

# Simple non palindrome
#    - Input: "john"
#    - Expected: "No"
run_test "john" "No"

echo "Test suite results: $PASSED/$NBCASES"
