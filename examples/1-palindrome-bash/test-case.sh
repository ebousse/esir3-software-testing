#!/bin/env bash

# Execute the program
RESULT=$(java -jar palindrome.jar "anna")

# Oracle
if [[ "$RESULT" == "Yes" ]]; then
  echo "Test [\"anna\", Yes] pass"
else
  echo "Test [\"anna\", Yes] failed"
fi
